from mymodules.models import Student
from mymodules.math_utils import average_grade

def main():
    student1 = Student("Bob", 95)
    student2 = Student("Bober", 88)
    student3 = Student("Bobert", 81)
    student4 = Student("Boberto", 74)
    student5 = Student("Boberito", 67)
    student6 = Student("Bobber", 80)
    student7 = Student("Bab", 99)
    student8 = Student("Berb", 30)
    student9 = Student("Bo", 85)
    student10 = Student("Boqb", 56)

    roster = [student1, student2, student3, student4, student5, student6, student7, student8, student9, student10,]

    return average_grade(roster)


print(f"The average grade of this class of ten students is {main()}")